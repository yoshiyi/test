package bean;

public class User implements java.io.Serializable {
    private String id;
    private String name;
    private String gender;
    private String term;
    private String lang;
    private String pass;

    public User() {

    }

    public User(String id, String name, String gender, String term, String lang, String pass) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.term = term;
        this.lang = lang;
        this.pass = pass;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

}