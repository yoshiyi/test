package service;

import bean.User;

public class RegisterUserLogic {

    // 問① 接続情報を記述してください
    /** ドライバーのクラス名 */
    private static final String POSTGRES_DRIVER = "org.postgresql.Driver";
    /** ・JDMC接続先情報 */
    private static final String JDBC_CONNECTION = "jdbc:postgresql://localhost:5432/User";
    /** ・ユーザー名 */
    private static final String USER = "postgres";
    /** ・パスワード */
    private static final String PASS = "postgres";
    /** ・タイムフォーマット */
    private static final String TIME_FORMAT = "yyyy/MM/dd HH:mm:ss";

    // 問② 入力された値で、UPDATEする文
    /** ・SQL create文 */
    private static final String SQL_INSERT = "INSERT INTO Usr_table SET login_time = ?  WHERE  id = ?";

//ユーザー登録を行うモデル(ファイルやデータベースへの登録は行わない)
    public boolean execute(User user) {
        // 登録処理
        return true;
    }
}